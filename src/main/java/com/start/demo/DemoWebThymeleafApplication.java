package com.start.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.start.controller.DemoController;

@SpringBootApplication
@ComponentScan(basePackageClasses = DemoController.class)
public class DemoWebThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoWebThymeleafApplication.class, args);
	}

}
